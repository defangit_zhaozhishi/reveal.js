# Flower Timeline

A Pen created on CodePen.io. Original URL: [https://codepen.io/borntofrappe/pen/qBdzaZG](https://codepen.io/borntofrappe/pen/qBdzaZG).

Calling it flower chain would actually be appropriate. Experiment with SVG syntax and anime.js to chain a series of animation.

I'm still uncertain as to how anime.js converts the coordinates to pixel values, but it seems to work.
