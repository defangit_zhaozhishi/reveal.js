# Day & Night Toggle

A Pen created on CodePen.io. Original URL: [https://codepen.io/borntofrappe/pen/aboPapm](https://codepen.io/borntofrappe/pen/aboPapm).

Practice with SVG syntax to recreate [this inspiring toggle](https://dribbble.com/shots/5321869-Playing-with-InVision-Studio).

## Credit

[Original design @BoMeetsWorld @KreativaStudio](https://dribbble.com/shots/5321869-Playing-with-InVision-Studio). They have an astronaut and a surfer to boot.
